package glfont;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GLFontData implements Serializable{

	private static final long serialVersionUID = 1848712526519830347L;

	//nb: coords are bottom left, not top left
	//ascent is HEIGHT - OFFSET_Y
	//descent is OFFSET_Y
	public static enum Metric { TEXTURE_X, TEXTURE_Y, WIDTH, HEIGHT, OFFSET_X, OFFSET_Y };
	
	private String fontName;
	private float spaceWidth;
	private int lineHeight;
	private HashMap<Integer,EnumMap<Metric,Integer>> cData;
	
	
	public GLFontData(){
		fontName = "Unnamed Font";
		spaceWidth = 0;
		
		cData = new HashMap<Integer,EnumMap<Metric,Integer>>();
	}
	
	public void setFontName(String fontName){
		this.fontName = fontName;
	}
	
	public void setSpaceWidth(float w){
		spaceWidth = w;
	}
	
	public void setLineHeight(int h){
		lineHeight = h;
	}
	
	public EnumMap<Metric,Integer> createCharMetrics(Integer charKey){
		EnumMap<Metric,Integer> charMetrics = new EnumMap<Metric,Integer>(Metric.class);
		cData.put(charKey, charMetrics);
		return charMetrics;
	}

	public float getSpaceWidth(){
		return spaceWidth;
	}	
	
	public int getLineHeight(){
		return lineHeight;
	}
	
	public EnumMap<Metric,Integer> getCharMetrics(Integer charKey){
		return cData.get(charKey);
	}
	
	
	//===========================================
	// SERIALIZATION
	//===========================================
	
	public static enum OutputFormat {
		DAT,  			/** Pipe ObjectOutputStream into GZipOutputStream **/
		JS,  			/** Write as JavaScript **/
		STDOUT_JS, 		/** Same format as JS, but only write to System.output **/
		CPP11,			/** Write as hpp/cpp (C++11) **/
		STDOUT_CPP11	/** Same format as CPP11, but only write to System.output **/
	};
	
	public void writeToFile(String fileOutFullBasename) throws IOException{
		writeToFile(fileOutFullBasename, OutputFormat.DAT);
	}
	public void writeToFile(String fileOutFullBasename, OutputFormat format) throws IOException{
		
		switch(format){
		case DAT: {
			File fileDAT = new File(fileOutFullBasename + ".dat");
			FileOutputStream fos = new FileOutputStream(fileDAT);
			this.writeToFileDat(fos);
			fos.close();
			return;
		}
		case JS: {
			File fileJS = new File(fileOutFullBasename + ".js");
			FileOutputStream fos = new FileOutputStream(fileJS);
			this.writeToFileJS(fos);
			fos.close();
			return;
		}
		case STDOUT_JS: {
			PrintStream stdout = System.out;
			stdout.println("[Writing JS data] Begin\n-------------------");
			this.writeToFileJS(stdout);
			stdout.println("\n-------------------\n[Writing JS data] Done!");
			return;
		}
		case CPP11: {
			File fileHPP = new File(fileOutFullBasename + ".hpp");
			FileOutputStream fosHPP = new FileOutputStream(fileHPP);
			this.writeToFileHPP(fosHPP);
			fosHPP.close();
			
			File fileCPP = new File(fileOutFullBasename + ".cpp");
			FileOutputStream fosCPP = new FileOutputStream(fileCPP);
			this.writeToFileCPP(fosCPP);
			fosCPP.close();
			
			return;
		}
		case STDOUT_CPP11: {
			PrintStream stdout = System.out;
			
			stdout.println("[Writing CPP11 (hpp) data] Begin\n-------------------");
			this.writeToFileHPP(stdout);
			stdout.println("\n-------------------\n[Writing CPP11 (hpp) data] Done!");
			
			stdout.println("[Writing CPP11 (cpp) data] Begin\n-------------------");
			this.writeToFileCPP(stdout);
			stdout.println("\n-------------------\n[Writing CPP11 (cpp) data] Done!");
			
			return;
		}
		default:
			System.err.println("Unknown output format!");
		}
	}
	
	//output formats
	//---------------------------------------------------------------------
	private void writeToFileDat(OutputStream os) throws IOException{
		
		GZIPOutputStream gzs = new GZIPOutputStream(os);
		ObjectOutputStream oos = new ObjectOutputStream(gzs);
		
		oos.writeObject(this);
		
		oos.close();
		gzs.close();
	}
	
	private void writeToFileJS(OutputStream os) throws IOException{
		String result = "";
		
		result += "// This file was generated automatically by GLFontData.\n";
		result += "// Git repository: https://bitbucket.org/tdmsrc/glfont \n";
		result += "\n";
		result += "const _M = {\n";
		
		Set<Integer> keys = cData.keySet();
		for(Integer i : keys){
			EnumMap<Metric,Integer> props = cData.get(i);
			
			int texX = props.get(Metric.TEXTURE_X);
			int texY = props.get(Metric.TEXTURE_Y);
			
			int w = props.get(Metric.WIDTH);
			int h = props.get(Metric.HEIGHT);
			
			int offX = props.get(Metric.OFFSET_X);
			int offY = props.get(Metric.OFFSET_Y);
			
			result += "  " + i + ": {\n";
			result += "    p: [" + texX + ", " + texY + "],\n";
			result += "    s: [" + w + ", " + h + "],\n";
			result += "    o: [" + offX + ", " + offY + "],\n";
			result += "  },\n";
		}
		
		result += "};\n";
		result += "\n";
		result += "const fontMetrics = {\n";
		result += "  fontName: '" + this.fontName + "',\n"; 
		result += "  spaceWidth: " + this.spaceWidth + ",\n";
		result += "  lineHeight: " + this.lineHeight + ",\n";
		result += "  getTexPos: k => _M[k].p,\n";
		result += "  getSize: k => _M[k].s,\n";
		result += "  getOffset: k => _M[k].o,\n";
		result += "};\n\n";
		result += "export default fontMetrics;";
		
		byte[] resultBytes = result.getBytes(Charset.forName("UTF-8"));
		os.write(resultBytes);
	}
	
	private void writeToFileHPP(OutputStream os) throws IOException{
		String result = "";
		
		result += "// This file was generated automatically by GLFontData.\n";
		result += "// Git repository: https://bitbucket.org/tdmsrc/glfont \n\n";
		result += "";
		
		//---------------------------------------
		result += "#ifndef _FONT_GEOMETRY_\n";
		result += "#define _FONT_GEOMETRY_\n";
		result += "\n";
		result += "#include <string>\n";
		result += "#include <unordered_map>\n";
		result += "\n";
		result += "struct CharMetric {\n";
		result += "    int textureX, textureY, width, height, offsetX, offsetY;\n";
		result += "};\n";
		result += "\n";
		result += "class FontGeometry\n";
		result += "{\n";
		result += "    protected:\n";
		result += "        const std::string fontName;\n";
		result += "        const float spaceWidth;\n";
		result += "        const int lineHeight;\n";
		result += "        const CharMetric nullCharMetric;\n";
		result += "        const std::unordered_map<int,CharMetric> charMetrics;\n";
		result += "\n";
		result += "    public:\n";
		result += "        FontGeometry();\n";
		result += "\n";
		result += "        float getSpaceWidth() const;\n";
		result += "        int getLineHeight() const;\n";
		result += "        const CharMetric& getCharMetric(const int c) const;\n";
		result += "};\n";
		result += "\n";
		result += "#endif\n";
		//---------------------------------------
		
		byte[] resultBytes = result.getBytes(Charset.forName("UTF-8"));
		os.write(resultBytes);
	}
	
	private void writeToFileCPP(OutputStream os) throws IOException{
		String result = "";
		
		result += "// This file was generated automatically by GLFontData.\n";
		result += "// Git repository: https://bitbucket.org/tdmsrc/glfont \n\n";
		
		//---------------------------------------
		result += "#include \"font_geometry.hpp\"\n";
		result += "\n";
		result += "FontGeometry::FontGeometry() :\n";
		result += "    fontName(\"" + this.fontName + "\"),\n";
		result += "    spaceWidth(" + this.spaceWidth + "),\n";
		result += "    lineHeight(" + this.lineHeight + "),\n";
		result += "    nullCharMetric({ 0,0,(int)spaceWidth,1,0,0 }),\n";
		result += "    charMetrics({\n";

		Set<Integer> keys = cData.keySet();
		int k = 0;
		for(Integer i : keys){
			EnumMap<Metric,Integer> props = cData.get(i);
			
			int texX = props.get(Metric.TEXTURE_X);
			int texY = props.get(Metric.TEXTURE_Y);
			
			int w = props.get(Metric.WIDTH);
			int h = props.get(Metric.HEIGHT);
			
			int offX = props.get(Metric.OFFSET_X);
			int offY = props.get(Metric.OFFSET_Y);
			
			String comma = (++k == keys.size()) ? "" : ",";  
			result += "        { " + i + ", {" + texX + "," + texY + "," + w + "," + h + "," + offX + "," + offY + "} }" + comma + "\n";
		}

		result += "    })\n";
		result += "{ }\n";
		result += "\n";
		result += "float FontGeometry::getSpaceWidth() const {\n";
		result += "    return spaceWidth;\n";
		result += "}\n";
		result += "\n";
		result += "int FontGeometry::getLineHeight() const {\n";
		result += "    return lineHeight;\n";
		result += "}\n";
		result += "\n";
		result += "const CharMetric& FontGeometry::getCharMetric(const int c) const\n";
		result += "{\n";
		result += "    const auto itMetric = charMetrics.find(c);\n";
		result += "    return (itMetric != charMetrics.end()) ? itMetric->second : nullCharMetric;\n";
		result += "}\n";
		//---------------------------------------
		
		byte[] resultBytes = result.getBytes(Charset.forName("UTF-8"));
		os.write(resultBytes);
	}
	//---------------------------------------------------------------------
	
	//read file output with format OutputFormat.DAT
	public static GLFontData readFromFile(InputStream fontDAT) throws ClassNotFoundException, IOException{

        GZIPInputStream zs = new GZIPInputStream(fontDAT);
        ObjectInputStream os = new ObjectInputStream(zs);
        
        GLFontData glFontData = (GLFontData)os.readObject();
        
        os.close();
        zs.close();
        
        return glFontData;
	}
}
