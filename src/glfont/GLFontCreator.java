package glfont;

import glfont.GLFontData.Metric;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;

import javax.imageio.ImageIO;


public class GLFontCreator{

	private Font font; //system font to use
	private int[] cAscii; //ascii values of chars to include
	
	//texture metrics
	private int textureWidth, textureHeight; //size of texture (will give error msg if yMax is too small)
	private int xPadding, yPadding; //minimum padding between individual characters and texture border
	
	//texture display options
	private boolean showDebugLines; //show bounding rects and baseline in the png
	private Color colorText, colorBackground;

	//output options
	private String fileOutPath = ""; //path to write output files (no trailing '/')
	private String fileOutBasename = "glfont"; //if e.g. "glfont", then the files "glfont.png" and "glfont.dat" would be written. "glfont.dat"
	
	
	public GLFontCreator(
		int textureWidth, int textureHeight,
		Color colorText, Color colorBackground, boolean transparentBackground){
		
		cAscii = GLFontCreator.getDefaultChars();
		
		font = Font.getFont("Serif"); //default font
		
		this.textureWidth = textureWidth; 
		this.textureHeight = textureHeight;
		xPadding = 2; yPadding = 2;
		
		showDebugLines = false;
		
		this.colorText = colorText;
		this.colorBackground = transparentBackground ?
			new Color(colorText.getRed(), colorText.getGreen(), colorText.getBlue(), 0) : colorBackground;
	}
	
	protected static int[] getDefaultChars(){
		
		//chars 32-255, omitting 127
		int[] cAscii = new int[223];
		
		int k = 0;
		for(int j=32; j<=255; j++){
			if(j == 127){ continue; }
			cAscii[k] = j;
			k++;
		}
		
		return cAscii;
	}
	
	public void setFont(Font font){
		this.font = font;
	}
	
	//get a full description of current font name
	protected String getFontFullName(){
		return font.getName() + (font.isBold() ? " (bold)" : "") + (font.isItalic() ? " (italic)" : "") + (" (size: " + font.getSize() + ")");
	}
	
	//set the file out path, if it is a valid directory
	public boolean setFileOutPath(String fileOutPath){
		File f = new File(fileOutPath);
		
		if(!f.isDirectory()){
			return false;
		}else{
			this.fileOutPath = fileOutPath;
			return true;
		}
	}
	
	//set the file out basename, if non-empty
	public boolean setFileOutBasename(String fileOutBasename){
		if(fileOutBasename.isEmpty()){
			return false;
		}else{
			this.fileOutBasename = fileOutBasename;
			return true;
		}
	}
	
	//check whether the file out info is ok
	private boolean checkFileOutInfo(){
		return (!this.fileOutPath.isEmpty() && !this.fileOutBasename.isEmpty());
	}
	
	private void debugMessage(boolean debugMessages, String msg){
		if(!debugMessages){ return; }
		
		System.out.println(msg);
	}
	
	//returns true if the font fits in the texture, false otherwise.
	public boolean generateGLFont(boolean outputFiles, boolean debugMessages){
		boolean fitsTexture = true; //set to false later if texture isn't big enough

		//prepare graphics and bufferedimage
		BufferedImage bufImg = new BufferedImage(textureWidth, textureHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bufImg.createGraphics();

		g.setRenderingHint(
			RenderingHints.KEY_TEXT_ANTIALIASING, 
			RenderingHints.VALUE_TEXT_ANTIALIAS_ON); //VALUE_TEXT_ANTIALIAS_GASP
		
		g.setFont(font);
		
		g.setBackground(colorBackground);
		g.clearRect(0, 0, textureWidth, textureHeight);
		
		//set up font data structure
		GLFontData fontData = new GLFontData();
		fontData.setFontName(getFontFullName());
		
		//space width
		TextLayout tlSpace = new TextLayout(" ", g.getFont(), g.getFontRenderContext());
		fontData.setSpaceWidth(tlSpace.getAdvance());
		
		//line height (only used for rendering later, not for drawing texture--must not have overlap)
		FontMetrics fm = g.getFontMetrics();
		fontData.setLineHeight(fm.getHeight());
		
		//obtain width, height, and offsets for each char
		int n = cAscii.length;

		for(int i=0; i<n; i++){
			int ascValue = cAscii[i];
			
			String sChar = new Character((char)ascValue).toString();
			TextLayout tl = new TextLayout(sChar, g.getFont(), g.getFontRenderContext());
			Rectangle cBounds = tl.getPixelBounds(g.getFontRenderContext(), 0, 0);
			
			EnumMap<Metric, Integer> charMetrics = fontData.createCharMetrics(ascValue);
			charMetrics.put(Metric.WIDTH, cBounds.width);
			charMetrics.put(Metric.HEIGHT, cBounds.height);
			charMetrics.put(Metric.OFFSET_X, cBounds.x);
			charMetrics.put(Metric.OFFSET_Y, cBounds.y + cBounds.height);
		}
		
		//generate texture and texture position data:
		// (progressively add characters to curLine until we reach max x value, /then/ process
		//  because the y position to draw is determined by ascent/descent of entire line)
		int x=xPadding, y=yPadding;
		
		ArrayList<Integer> curLine = new ArrayList<Integer>();
		curLine.clear();
		
		for(int i=0; i<n; i++){
			
			int ascValue = cAscii[i];
			EnumMap<Metric,Integer> charMetrics = fontData.getCharMetrics(ascValue);
		
			int cWidth = charMetrics.get(Metric.WIDTH);
			if((x + cWidth + xPadding) > textureWidth){
				x = xPadding;
				y = processLine(curLine, y, fontData, g) + yPadding;
				curLine.clear();
			}
			
			curLine.add(ascValue);
			x += cWidth + xPadding;
		}
		
		y = processLine(curLine, y, fontData, g) + yPadding;
		
		if(y > textureHeight){ fitsTexture = false; }
		
		//output metric data in console
		for(int i=0; i<n; i++){
			
			int ascValue = cAscii[i];
			EnumMap<Metric,Integer> charMetrics = fontData.getCharMetrics(ascValue);
			
			debugMessage(debugMessages, 
				"ASCII: " + ascValue + " (" + (new Character((char)ascValue).toString()) + ");  " + 
				"TEX (X,Y) = " + charMetrics.get(Metric.TEXTURE_X) + ", " + charMetrics.get(Metric.TEXTURE_Y) + ";  " + 
				"(W,H) = " + charMetrics.get(Metric.WIDTH) + ", " + charMetrics.get(Metric.HEIGHT) + ";  " + 
				"OFF (X,Y) = " + charMetrics.get(Metric.OFFSET_X) + ", " + charMetrics.get(Metric.OFFSET_Y)
			);
		}
		debugMessage(debugMessages, "Space Width: " + fontData.getSpaceWidth());
		debugMessage(debugMessages, "Line Height: " + fontData.getLineHeight());
		
		//do not output files unless outputFiles == true
		if(!outputFiles){ return fitsTexture; }
		
		//make sure file output info is OK
		if(!this.checkFileOutInfo()){ 
			System.err.println("Invalid file output settings!");
			return true; 
		}
		
		String fileOutPNG = this.fileOutPath + "/" + this.fileOutBasename + ".png";
		String fileOutFullBasename = this.fileOutPath + "/" + this.fileOutBasename;
		
		//output metric data as file
		try{
			//TODO allow format to be set in the UI
			fontData.writeToFile(fileOutFullBasename, GLFontData.OutputFormat.JS);
		}catch(IOException e){
			System.err.println(e.getMessage());
		}
		
		//output texture as png
		try{
			File filePNG = new File(fileOutPNG);
			ImageIO.write(bufImg, "png", filePNG);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
		return fitsTexture;
	}
	
	protected int processLine(ArrayList<Integer> curLine, int y, GLFontData fontData, Graphics2D g){
		
		//draws curline and adds texture coordinates to the char metric data
		//y value is at the bottom of the previous line; return new y

		//get line ascent and descent
		int lineAscent=0, lineDescent=0;

		for(int i=0; i<curLine.size(); i++){
			int ascValue = curLine.get(i);
			EnumMap<Metric,Integer> charMetrics = fontData.getCharMetrics(ascValue);
			
			int charDescent = charMetrics.get(Metric.OFFSET_Y);
			int charAscent = charMetrics.get(Metric.HEIGHT) - charMetrics.get(Metric.OFFSET_Y);

			if((charAscent > lineAscent) || (i==0)){ lineAscent = charAscent; }
			if((charDescent > lineDescent) || (i==0)){ lineDescent = charDescent; }
		}
		
		//draw the texure and record metrics
		int x = xPadding;
		int yBaseline = y + lineAscent;
		
		for(int i=0; i<curLine.size(); i++){
			int ascValue = curLine.get(i);
			EnumMap<Metric,Integer> charMetrics = fontData.getCharMetrics(ascValue);
			
			int charDescent = charMetrics.get(Metric.OFFSET_Y);
			int charAscent = charMetrics.get(Metric.HEIGHT) - charMetrics.get(Metric.OFFSET_Y);
			
			//draw the current char
			String sChar = new Character((char)ascValue).toString();
			//[TODO]
			g.setColor(colorText);
			g.drawString(sChar, x-charMetrics.get(Metric.OFFSET_X), yBaseline);
			
			//draw char bounding box (if enabled)
			if(showDebugLines){
				g.setColor(new Color(1.0f, 0.0f, 0.0f, 0.6f));
				g.drawRect(x, yBaseline-charAscent, charMetrics.get(Metric.WIDTH), charMetrics.get(Metric.HEIGHT));
			}
			
			//add lower-left texture coords for this char
			charMetrics.put(Metric.TEXTURE_X, x);
			charMetrics.put(Metric.TEXTURE_Y, yBaseline + charDescent);
			
			//update x position
			x += charMetrics.get(Metric.WIDTH) + xPadding;
		}
		
		//draw baseline (if enabled)
		if(showDebugLines){
			g.setColor(new Color(0.0f, 0.0f, 1.0f, 0.6f));
			g.drawLine(0,yBaseline,x,yBaseline);
		}

		return y + lineAscent + lineDescent;
	}
}
