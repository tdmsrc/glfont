package glfont;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class GLFontApp extends JFrame{
	private static final long serialVersionUID = 1L;
	
	//TODO allow these to be set in the UI
	
	private static final Color COLOR_TEXT = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	private static final Color COLOR_BACKGROUND = new Color(0.0f, 0.0f, 0.0f, 1.0f);
	private static final boolean TRANSPARENT_BACKGROUND = true;
	
	private static final int TEX_WIDTH = 1024;
	private static final int TEX_HEIGHT = 1024;
	
	//current font as selected by UI
	private String curFontName; //font name and size will be set during generation of UI components
	private int curFontSize;
	private boolean curFontBold = false;
	private boolean curFontItalic = false;
	
	//label to display chosen font
	private JLabel labelFontSample;
	
	//font creator
	private GLFontCreator glFontCreator = new GLFontCreator(
		TEX_WIDTH, TEX_HEIGHT, 
		COLOR_TEXT, COLOR_BACKGROUND, TRANSPARENT_BACKGROUND);
	
	public static void main(String[] args){
		
		GLFontApp frame = new GLFontApp("GL Font");
		frame.setupGUI();
	}
	
	public GLFontApp(String s){
		super(s);

		addWindowListener(new WindowAdapter(){
	    	public void windowClosing(WindowEvent e) { System.exit(0); }
	    });
	}
	
	public void updateFont(){
		
		int fontStyle = Font.PLAIN;
		if(curFontBold){ fontStyle = fontStyle | Font.BOLD; }
		if(curFontItalic){ fontStyle = fontStyle | Font.ITALIC; }
		
		Font font = new Font(curFontName, fontStyle, curFontSize);
		
		glFontCreator.setFont(font);
		labelFontSample.setFont(font);
	}
	
	//UI components relating to font: add to target
	private void setupGUIFont(Container target){
		
		//get font list
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();  
		String[] strFonts = ge.getAvailableFontFamilyNames(); 
		
		//make list of fonts
		JList<String> jl = new JList<String>(strFonts);
		jl.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jl.setSelectedIndex(0);
		curFontName = (String)jl.getSelectedValue();
		jl.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				if(e.getValueIsAdjusting()){ return; }
				JList jl = (JList)e.getSource();
				curFontName = (String)jl.getSelectedValue();
				updateFont();
			}
		});
		
		JScrollPane spFonts = new JScrollPane(jl);
		target.add(spFonts);
	}
	

	//UI components relating to font options: add to target
	private void setupGUIFontOptions(Container target){
		
		//size
		Integer[] fontSizes = new Integer[]{ 12, 14, 16, 18, 20, 22, 24, 26, 28, 32, 36, 40, 44, 48, 54, 60, 66, 72, 80, 88, 96 };
		JComboBox<Integer> cb = new JComboBox<Integer>(fontSizes);
		
		cb.setSelectedIndex(0);
		curFontSize = (Integer)cb.getSelectedItem();
		cb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        JComboBox cb = (JComboBox)e.getSource();
		        curFontSize = (Integer)cb.getSelectedItem();
		        updateFont();
		    }
	
		});
		target.add(cb);
		
		//checkboxes for bold and italic
		JCheckBox chkBold = new JCheckBox("Bold", curFontBold);
		chkBold.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				curFontBold = (e.getStateChange() == ItemEvent.SELECTED);
				updateFont();
			}
		});
		target.add(chkBold);
		
		JCheckBox chkItalic = new JCheckBox("Italic", curFontItalic);
		chkItalic.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				curFontItalic = (e.getStateChange() == ItemEvent.SELECTED);
				updateFont();
			}
		});
		target.add(chkItalic);
		
		//test button
		JButton btn;
		
		btn = new JButton("Test");
		btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){ 
				if(!glFontCreator.generateGLFont(false, false)){
					System.err.println("Texture is not big enough.");
				}
				else{ System.out.println("Texture size is ok."); }
			}
		});
		target.add(btn);
	}
	
	//UI components for completing actions: add to target
	private void setupGUIActions(final Container target){
		
		JButton btn;
		
		//directory chooser
		btn = new JButton("Output Dir...");
		btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){ 
				//prepare file chooser
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Choose an output directory");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				
				//open file chooser and set result
				int result = chooser.showOpenDialog(target);
				if(result == JFileChooser.APPROVE_OPTION){
					try{
						String fileOutPath = chooser.getSelectedFile().getCanonicalPath();
						glFontCreator.setFileOutPath(fileOutPath);
						System.out.println("Selected directory: \"" + fileOutPath + "\"");
					}catch(IOException err){
						System.err.println("Problem selecting directory: " + err.getMessage());
					}
				}
			}
		});
		target.add(btn);
		
		//button to generate
		btn = new JButton("Generate");
		btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){ 
				if(!glFontCreator.generateGLFont(true, true)){
					System.err.println("Texture is not big enough.");
				}
				else{ System.out.println("Texture size is ok."); }
			}
		});
		target.add(btn);
	}
	
	//set layout and add all UI components
	public void setupGUI(){
		
		setLayout(new BorderLayout());
		
		int uiPanelWidth = 260;
		
		//label with sample text
		JPanel panelFontTexture = new JPanel();
		add(panelFontTexture, BorderLayout.CENTER);
		
		//font sample at the top
		labelFontSample = new JLabel("<html>Sample Text<br>ABCDEFGHIJKLMNOPQRSTUVWXYZ<br>abcdefghijklmnopqrstuvwxyz<br>1234567890!@#$%^&*(){}[]</html>");
		panelFontTexture.add(labelFontSample);
		
		//left panel, containing options components
		JPanel leftPanel = new JPanel(new FlowLayout());
		leftPanel.setPreferredSize(new Dimension(uiPanelWidth, 1024));
		add(leftPanel, BorderLayout.LINE_START);
		
		//font panel
		JPanel fontPanel = new JPanel(new FlowLayout());
		
		TitledBorder fontTitle;
		fontTitle = BorderFactory.createTitledBorder("Font");
		fontPanel.setBorder(fontTitle);
		
		leftPanel.add(fontPanel);
		this.setupGUIFont(fontPanel);
		
		//font options panel
		JPanel fontOptsPanel = new JPanel(new FlowLayout());
		
		TitledBorder fontOptsTitle;
		fontOptsTitle = BorderFactory.createTitledBorder("Font Options");
		fontOptsPanel.setBorder(fontOptsTitle);
		
		leftPanel.add(fontOptsPanel);
		this.setupGUIFontOptions(fontOptsPanel);
		
		//actions panel
		JPanel actionsPanel = new JPanel(new FlowLayout());
		
		TitledBorder actionsTitle;
		actionsTitle = BorderFactory.createTitledBorder("Actions");
		actionsPanel.setBorder(actionsTitle);
		
		leftPanel.add(actionsPanel);
		this.setupGUIActions(actionsPanel);
	    
		//finish up
		setSize(uiPanelWidth+512, 300);
	    setVisible(true);
	    
	    updateFont();
	}
}
